/**
 * Header
 **/

#include <cctype>

#include <limits>
#include <string>

#include <stack>

#include <iostream>

#include <math.h>

using namespace std;

/**
 * Functions
 **/

bool isOperator(char character)
{
	return (character == '+' || character == '-' || character == '*' || character == '/' || character == '^');
}