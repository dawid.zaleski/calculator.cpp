#include "header.cpp"

#include "convert.cpp"
#include "calculate.cpp"

int main()
{
	system("@cls||clear");

	cout << endl;

	while (1)
	{
		cout << "OPERATION: ";

		string input = "";

		cin >> input;

		system("@cls||clear");

		try
		{
			Converter a;

			string reverse_Polish_notation = a.convert(input);

			Calculator b;

			double result = b.calculate(reverse_Polish_notation);

			cout << "RESULT: ";

			for (int i = 0, j = input.length(); i < j; i++)
				if (!isspace(input[i]))
					if (input[i] != ',')
						cout << input[i];
					else
						cout << '.';

			cout << " = " << (double) result << endl;
		}
		catch (const char* message)
		{
			cout << "ERROR: " << message << endl;
		}
	}

	return 0;
}
