/**
 * Convert
 **/

class Converter
{

public:
	string convert(string input)
	{
		stack <char> root;

		string output;

		int check = 0, power = 0;

		for (int i = 0, j = input.length(); i < j; i++)
		{
			if (i != j - 1)
			{
				if ((i == 0 ? true : !isdigit(input[i - 1])) && input[i] == '-' && isdigit(input[i + 1]))
				{
					output.push_back('-');
					continue;
				}

				if(i != 0)
					if (isdigit(input[i - 1]) && (input[i] == '.' || input[i] == ',') && isdigit(input[i + 1]))
					{
						output.push_back('.'), check = 0;
						continue;
					}
			}

			if (isdigit(input[i]))
			{
				if (check >= 8)
					throw "Integer overflow occurred.";

				output.push_back(input[i]), check++;
				continue;
			}

			if (i != 0)
				if (isdigit(input[i - 1]))
					output.push_back(' '), check = 0;

			if (isOperator(input[i]) && i < j - 1 && i != 0)
			{
				if (isOperator(input[i + 1]))
					throw "An unexpected character occurred in the input string.";

				if (input[i] == '^')
					power++;
				else
					power = 0;

				if (power > 1)
					throw "An unexpected character occurred in the input string.";

				if (!root.empty())
					while (((priority('^') < priority(root.top())) && input[i] == '^') || ((priority(input[i]) <= priority(root.top())) && input[i] != '^'))
					{
						output.push_back(input[i]), output.push_back(' '), root.pop();
					}

				root.push(input[i]);
				continue;
			}

			if (input[i] == '(')
			{
				root.push('(');
				continue;
			}

			if (input[i] == ')')
			{
				while (root.top() != '(')
				{
					if (root.empty())
						throw "Incorrect input.";

					output.push_back(input[i]), output.push_back(' '), root.pop();
				}

				root.pop();
				continue;
			}

			if (isspace(input[i]))
				continue;

			throw "An unexpected character occurred in the input string.";
		}

		if (!root.empty())
		{
			output.push_back(' ');

			do
			{
				if (!isOperator(root.top()))
					throw "Incorrect input.";

				output.push_back(root.top()), output.push_back(' '), root.pop();
			} while (!root.empty());
		}

		if (output.length() > 0)
			return output;

		throw "No output data.";
	}

private:
	int priority(char character)
	{
		if (character == '^')
			return 3;

		if (character == '*' || character == '/')
			return 2;

		if (character == '+' || character == '-')
			return 1;

		return 0;
	}
};