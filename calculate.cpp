/**
 * Calculate
 **/

class Calculator
{
public:
	double calculate(string input)
	{
		stack <double> root;

		string temporary = "";

		for (int i = 0, j = input.length(); i < j; i++)
		{
			if (input[i] == '-' && isdigit(input[i + 1]))
			{
				temporary.push_back('-');
				continue;
			}

			if (input[i] == '.')
			{
				temporary.push_back('.');
				continue;
			}

			if (isdigit(input[i]))
			{
				temporary.push_back(input[i]);
				continue;
			}

			if (temporary.length() > 0)
				root.push(stod(temporary)), temporary = "";

			if (isOperator(input[i]))
			{
				double result;

				double a = root.top();
				root.pop();
				double b = root.top();
				root.pop();

				switch (input[i])
				{
					case '+':
						if ((a > 0 && a > INT_MAX - b) || (a < 0 && a < INT_MIN - b))
							throw "Integer overflow occurred.";

						result = a + b;
						break;

					case '-':
						if ((b > 0 && b > INT_MAX + a) || (b < 0 && b < INT_MIN + a))
							throw "Integer overflow occurred.";

						result = b - a;
						break;

					case '*':
						if ((a > 0 && a > INT_MAX / b) || (a < 0 && a < INT_MIN / b))
							throw "Integer overflow occurred.";

						result = a * b;
						break;

					case '/':
						if (a == 0)
							throw  "Don't divide by zero.";

						if ((b > 0 && b > INT_MAX * a) || (b < 0 && b < INT_MIN * a))
							throw "Integer overflow occurred.";

						result = b / a;
						break;

					case '^':
						result = pow(b, a);

						if (isnan(result) || isinf(result))
							throw "Integer overflow occurred.";

						break;

					default:
						throw "An unexpected character occurred in the input string.";
				}

				root.push(result);
				continue;
			}

			if (isspace(input[i]))
				continue;

			throw "An unexpected character occurred in the input string.";
		}

		if (temporary.length() != 0)
			return stod(temporary);

		return root.top();
	}
};
